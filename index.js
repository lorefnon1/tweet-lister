const minimist = require('minimist');
const axios = require('axios');

/**
 * Primary entry point for application
 */
const main = async () => {
    const args = parseArgs();
    const results = await fetchTweets(args);
    printResults(results);
};

/**
 * Parse command line arguments
 *
 * Throws when mandatory arguments (count (or its alias n) and query (or its alias q)) are not present
 */
const parseArgs = () => {
    var argv = minimist(process.argv.slice(2));
    let count = argv.n || argv.count;
    const query = argv.q || argv.query;
    if (!count || !query) {
        throw new Error(
            'count and query parameters must be specified.\n' +
            'Example usage: node index.js --count=10 --query=iPhone'
        );
    }
    if (isNaN(count)) {
        throw new Error('count argument must be a number');
    }
    count = Number(count);
    if (count > 100) {
        throw new Error(`count > 100 is currently not supported by Twitter Search API`);
    }
    if (query.length > 500) {
        throw new Error(`Queries of length > 500 chars is currently not supported by Twitter Search API`);
    }
    if (!process.env.BEARER_ACCESS_TOKEN) {
        throw new Error(`BEARER_ACCESS_TOKEN environment variable must be available`);
    }
    return { count, query };
}

/**
 * Fetch list of tweets using the Standard Search API
 *
 * @param {Object} options
 * @param {Number} options.count Number of tweets to be fetched
 * @param {String} options.query The search query
 *
 * @see https://developer.twitter.com/en/docs/tweets/search/api-reference/get-search-tweets
 */
const fetchTweets = async ({ count, query }) => {
    try {
        const response = await axios({
            url: 'https://api.twitter.com/1.1/search/tweets.json',
            method: 'get',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${process.env.BEARER_ACCESS_TOKEN}`
            },
            params: {
                q: query,
                count
            }
        });
        return response.data;
    } catch (e) {
        console.error(e);
        throw new Error('Failed to fetch tweets');
    }
}

/**
 * Print results retrieved from twitter search API
 *
 * @param {{statuses: {text: string}[]}} results
 */
const printResults = async (results) => {
    console.log(`Displaying ${results.statuses.length} tweets below:\n`);
    for (const status of results.statuses) {
        const username = (status.user && status.user.name) || 'Unknown';
        console.log(`---[ By @${username} ]---\n${status.text}\n`);
    }
}

main().catch(err => {
    console.log(err.message);
    process.exit(1);
});