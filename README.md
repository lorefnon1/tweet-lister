# About

A script to fetch list of tweets maching a search criteria

# Setup

## Installing dependencies

1. Ensure node.js is installed ([Instructions](https://nodejs.org/en/))
2. Run `npm install` or `yarn` in the project root to install project dependencies

## Notes about dependencies:

1. The script has been tested with Node.js v13.10.1 (Versions older than that may not work)
2. Following libraries are used:
  - [minimist](https://www.npmjs.com/package/minimist): For argument parsing
  - [axios](https://www.npmjs.com/package/axios): For making HTTP request

## Setting up Twitter developer account

1. A twitter developer account is required. Please setup an account as per instructions in the [official docs](https://developer.twitter.com/en/docs/basics/getting-started)
2. Create a twitter developer app. Refer [instructions](https://developer.twitter.com/en/docs/basics/apps/overview) and save the API Key and API secret key.
3. Generate an OAuth 2 Bearer token:

```
curl -u '<API key>:<API secret key>' --data 'grant_type=client_credentials' 'https://api.twitter.com/oauth2/token'

# Example:
curl -u 'FtglRMkafjvPXhIM8qlcP59bi:2M3V6ZjwHWmVsU6PrkmrEqlA9FDjjHHlyxAWGbQW9etBYbWfg0'   --data 'grant_type=client_credentials' 'https://api.twitter.com/oauth2/token'

# Sample response:
{"token_type":"bearer","access_token":"AAAAAAAAAAAAAAAAAAAAANZZDQEAAAAABXPhGYchEQMqZCY0XFTovpTJNqI%3DdD66FfLlDyT4iiTfIfgpDYGergV4sWXmthrag7K8hMnsO0opXb"}
```
4. Export the access token in the response above:

```
export BEARER_ACCESS_TOKEN='AAAAAAAAAAAAAAAAAAAAANZZDQEAAAAABXPhGYchEQMqZCY0XFTovpTJNqI%3DdD66FfLlDyT4iiTfIfgpDYGergV4sWXmthrag7K8hMnsO0opXb'
```

# Running the script:

Run: `node index.js --count=10 --query=iPhone`
